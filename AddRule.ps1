# AddRule

Function AddRule {
  Param(
    $TargetPath = "",
    $TargetExecutable = "",
    $Service,
    $Enabled = "True",
    $DisplayName,
    $Description,
    $Group,
    $Direction,
    $Protocol = "Any",
    $RemoteAddress,
    $RemotePort,
    $Action = "Allow"
  )

#  if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
#    Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`" $args" -Verb RunAs;
#    exit 
#  }

  if ($TargetPath) {
    $Program = Get-ChildItem $TargetPath -ErrorAction SilentlyContinue -Filter $TargetExecutable -Recurse | Select -last 1 | % { $_.FullName }
  } else {
    $Program = $TargetExecutable
  }

  if (!$Program)
    {
      Write-Host "Target not found!" -ForegroundColor 'Red'
      Read-Host
      Exit
    }

  Write-Host "Found: $Program" -ForegroundColor 'Green'
  #Write-Host
  #Exit

  $OldRules = Get-NetFirewallRule -DisplayName $DisplayName -ErrorAction SilentlyContinue
  if ($OldRules)
    {
      Write-Host "Removing old rules..." # -ForegroundColor 'Red'

      Remove-NetFirewallRule `
        -DisplayName $DisplayName `
        -ErrorAction SilentlyContinue
    }

  Write-Host "Adding new firewall rule:" -ForegroundColor 'Green'

  if ($Service)
    {
    New-NetFirewallRule `
      -Enabled $Enabled `
      -DisplayName $DisplayName `
      -Description $Description `
      -Group $Group `
      -Program $Program `
      -Service $Service `
      -Direction $Direction `
      -Protocol $Protocol `
      -RemoteAddress $RemoteAddress `
      -RemotePort $RemotePort `
      -Action $Action
    }
  else {
    New-NetFirewallRule `
      -Enabled $Enabled `
      -DisplayName $DisplayName `
      -Description $Description `
      -Group $Group `
      -Program $Program `
      -Direction $Direction `
      -Protocol $Protocol `
      -RemoteAddress $RemoteAddress `
      -RemotePort $RemotePort `
      -Action $Action
  }
}

Function Finish {
  Write-Host "Press ENTER to exit" -ForegroundColor 'Green'
  Read-Host
}



