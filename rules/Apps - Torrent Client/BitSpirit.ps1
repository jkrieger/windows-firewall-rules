# BitSpirit

$Group = "Apps - Torrent - BitSpirit"
$Program = "%ProgramFiles(x86)%\BitSpirit\BitSpirit.exe"


New-NetFirewallRule `
   -Enabled True `
   -DisplayName "BitSpirit (Out)" `
   -Description "Allow all outbound BitSpirit traffic" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Action Allow


New-NetFirewallRule `
   -Enabled True `
   -DisplayName "BitSpirit (In)" `
   -Description "Allow all inbound BitSpirit traffic" `
   -Group $Group `
   -Program $Program `
   -Direction Inbound `
   -Action Allow

