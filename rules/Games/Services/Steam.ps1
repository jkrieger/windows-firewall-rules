# Steam

$Group = "Games - Service - Steam"
$Program = "G:\_steam_\steam.exe"


New-NetFirewallRule `
   -Enabled True `
   -DisplayName "Steam Core" `
   -Description "Allow Steam Core traffic to all ports" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Protocol Any `
   -Action Allow


$Program = "G:\_steam_\bin\cef\cef.win7x64\steamwebhelper.exe"

New-NetFirewallRule `
   -Enabled True `
   -DisplayName "Steam Client Bootstrapper" `
   -Description "Allow Steam Client Bootstrapper traffic to all ports" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Protocol Any `
   -Action Allow


$Program = "G:\_steam_\steamerrorreporter.exe"

New-NetFirewallRule `
   -Enabled True `
   -DisplayName "Steam Error Reporter" `
   -Description "Allow Steam Error Reporter traffic to all ports" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Protocol Any `
   -Action Allow


$Program = "%ProgramFiles(x86)%\common files\steam\steamservice.exe"

New-NetFirewallRule `
   -Enabled True `
   -DisplayName "Steam Client Service" `
   -Description "Allow Steam Client Service traffic to all ports" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Protocol Any `
   -Action Allow
