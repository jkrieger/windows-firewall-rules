# Battle.net

$Group = "Games - Service - Battle.net"
#$Program = "G:\_battle.net_\battle.net\battle.net.exe"
$Program = Get-ChildItem -path g:\ -recurse -include "battle.net.exe" -erroraction SilentlyContinue

New-NetFirewallRule `
   -Enabled True `
   -DisplayName "Battle.net" `
   -Description "Allow Battle.net traffic to all ports" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Protocol Any `
   -Action Allow


#$Program = "C:\programdata\battle.net\agent\agent.7380\agent.exe"
$Program = Get-ChildItem -path "C:\programdata\battle.net\agent\agent.*\agent.exe" -recurse -include "agent.exe" -erroraction SilentlyContinue | Select -last 1

New-NetFirewallRule `
   -Enabled True `
   -DisplayName "Battle.net Update Agent" `
   -Description "Allow Battle.net Update Agent traffic to all ports" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Protocol Any `
   -Action Allow

