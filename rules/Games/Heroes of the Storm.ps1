# Heroes of the Storm
. ..\..\AddRule.ps1

$Rule = @{
  TargetPath = "g:\"
  TargetExecutable = "Heroes of the Storm.exe"

  Group = "Games - Heroes of the Storm"
  DisplayName = "Heroes of the Storm"
  Description = "Allow Heroes of the Storm x64 traffic to all ports"
  Direction = "Outbound"
  Protocol = "Any"
  Action = "Allow"
  Enabled = "True"
}

AddRule @Rule

$Rule = @{
  TargetPath = "g:\"
  TargetExecutable = "heroesofthestorm_x64.exe"

  Group = "Games - Heroes of the Storm"
  DisplayName = "Heroes of the Storm x64"
  Description = "Allow Heroes of the Storm x64 traffic to all ports"
  Direction = "Outbound"
  Protocol = "Any"
  Action = "Allow"
  Enabled = "True"
}

AddRule @Rule

Finish