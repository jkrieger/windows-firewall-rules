# KeePass

$Group = "Apps - Security - KeePass"
$Program = "%ProgramFiles(x86)%\keepass\keepass.exe"


New-NetFirewallRule `
   -Enabled True `
   -DisplayName "KeePass HTTP(S)" `
   -Description "Allow KeePass HTTP(S) traffic" `
   -Group $Group `
   -Program $Program `
   -Direction Outbound `
   -Protocol TCP `
   -RemotePort 80,443 `
   -Action Allow
