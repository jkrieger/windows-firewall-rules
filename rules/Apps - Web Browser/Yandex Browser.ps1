# Yandex Browser
. ..\..\AddRule.ps1

$Group = "Apps - Web Browser - Yandex Browser"
$TargetPath = "$env:LocalAppData\Yandex\YandexBrowser\Application\"
$TargetExecutable = "browser.exe"


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Yandex Browser HTTP(S)"
  Description = "Allow Yandex Browser HTTP(S) traffic"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Yandex Browser All TCP Ports"
  Description = "Allow Yandex Browser traffic to all ports"

  Direction = "Outbound"
  Protocol = "TCP"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Yandex Browser QUIC"
  Description = "Allow Yandex Browser QUIC traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Yandex Browser (mDNS-In)"
  Description = "Allow Yandex Browser mDNS inbound traffic"

  Direction = "Inbound"
  Protocol = "UDP"
  RemotePort = "5353"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Yandex Browser (SSDP-Out)"
  Description = "Allow Yandex Browser SSDP traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemoteAddress = "239.255.255.250"
  RemotePort = "1900"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


#$Rule = @{
#  Group = $Group
#  TargetExecutable = $Program
#
#  DisplayName = "Yandex Browser Sync Settings"
#  Description = "Allow Yandex Browser Sync Settings traffic"
#
#  Direction = "Outbound"
#  Protocol = "TCP"
#  RemotePort = "5228"
#  Action = "Allow"
#}
#
#AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = "service_update.exe"

  DisplayName = "Yandex Browser Update"
  Description = "Allow Yandex Browser Update"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "443"
  Action = "Allow"
}

AddRule @Rule
