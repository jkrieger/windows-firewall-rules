# Chromium Browser
. ..\..\AddRule.ps1

$Group = "Apps - Web Browser - Chromium"
$TargetPath = "$env:LocalAppData\Chromium\Application"
$TargetExecutable = "chrome.exe"


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Chromium Browser HTTP(S)"
  Description = "Allow Chromium Browser HTTP(S) traffic"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Chromium Browser All TCP Ports"
  Description = "Allow Chromium Browser traffic to all ports"

  Direction = "Outbound"
  Protocol = "TCP"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Chromium Browser QUIC"
  Description = "Allow Chromium Browser QUIC traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Chromium Browser (mDNS-In)"
  Description = "Allow Chromium Browser mDNS inbound traffic"

  Direction = "Inbound"
  Protocol = "UDP"
  RemotePort = "5353"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Chromium Browser (SSDP-Out)"
  Description = "Allow Chromium Browser SSDP traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemoteAddress = "239.255.255.250"
  RemotePort = "1900"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


#$Rule = @{
#  Group = $Group
#  TargetExecutable = $Program
#
#  DisplayName = "Chromium Browser Sync Settings"
#  Description = "Allow Chromium Browser Sync Settings traffic"
#
#  Direction = "Outbound"
#  Protocol = "TCP"
#  RemotePort = "5228"
#  Action = "Allow"
#}
#
#AddRule @Rule


#$Rule = @{
#  Group = $Group
#  TargetPath = $TargetPath
#  TargetExecutable = "service_update.exe"
#
#  DisplayName = "Chromium Browser Update"
#  Description = "Allow Chromium Browser Update"
#
#  Direction = "Outbound"
#  Protocol = "TCP"
#  RemotePort = "443"
#  Action = "Allow"
#}
#
#AddRule @Rule
