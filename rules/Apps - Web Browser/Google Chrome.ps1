# Google Chrome
. ..\..\AddRule.ps1

$Group = "Apps - Web Browser - Google Chrome"
$TargetPath = "$env:ProgramFiles(x86)\Chromium\Application"
$TargetExecutable = "chrome.exe"


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Google Chrome HTTP(S)"
  Description = "Allow Google Chrome HTTP(S) traffic"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Google Chrome All TCP Ports"
  Description = "Allow Google Chrome traffic to all ports"

  Direction = "Outbound"
  Protocol = "TCP"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Google Chrome QUIC"
  Description = "Allow Google Chrome QUIC traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Google Chrome (mDNS-In)"
  Description = "Allow Google Chrome mDNS inbound traffic"

  Direction = "Inbound"
  Protocol = "UDP"
  RemotePort = "5353"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Google Chrome (SSDP-Out)"
  Description = "Allow Google Chrome SSDP traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemoteAddress = "239.255.255.250"
  RemotePort = "1900"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Google Chrome Sync Settings"
  Description = "Allow Google Chrome Sync Settings traffic"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "5228"
  Action = "Allow"
}

AddRule @Rule


$TargetPath = "$env:ProgramFiles(x86)\Google\Update"
$TargetExecutable = "googleupdate.exe"


$Rule = @{
  Group = $Group
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Google Chrome Update"
  Description = "Allow Google Chrome Update"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "443"
  Action = "Allow"
}

AddRule @Rule
