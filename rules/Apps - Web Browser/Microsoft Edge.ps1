# Microsoft Edge
. ..\..\AddRule.ps1

$Group = "Apps - Web Browser - Microsoft Edge"
$Program = "%ProgramFiles(x86)%\Microsoft\Edge\Application\msedge.exe"

$Rule = @{
  Group = $Group
  TargetExecutable = $Program

  DisplayName = "Microsoft Edge HTTP(S)"
  Description = "Allow Microsoft Edge HTTP(S) traffic"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetExecutable = $Program

  DisplayName = "Microsoft Edge All TCP Ports"
  Description = "Allow Microsoft Edge traffic to all ports"

  Direction = "Outbound"
  Protocol = "TCP"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetExecutable = $Program

  DisplayName = "Microsoft Edge QUIC"
  Description = "Allow Microsoft Edge QUIC traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetExecutable = $Program

  DisplayName = "Microsoft Edge (mDNS-In)"
  Description = "Allow Microsoft Edge mDNS inbound traffic"

  Direction = "Inbound"
  Protocol = "UDP"
  RemotePort = "5353"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  Group = $Group
  TargetExecutable = $Program

  DisplayName = "Microsoft Edge (SSDP-Out)"
  Description = "Allow Microsoft Edge SSDP traffic"

  Direction = "Outbound"
  Protocol = "UDP"
  RemoteAddress = "239.255.255.250"
  RemotePort = "1900"
  Action = "Allow"
  Enabled = "False"
}

AddRule @Rule


#$Rule = @{
#  Group = $Group
#  TargetExecutable = $Program
#
#  DisplayName = "Microsoft Edge Sync Settings"
#  Description = "Allow Microsoft Edge Sync Settings traffic"
#
#  Direction = "Outbound"
#  Protocol = "TCP"
#  RemotePort = "5228"
#  Action = "Allow"
#}
#
#AddRule @Rule


$Program = "%ProgramFiles(x86)%\Microsoft\EdgeUpdate\MicrosoftEdgeUpdate.exe"


$Rule = @{
  Group = $Group
  TargetExecutable = $Program

  DisplayName = "Microsoft Edge Update"
  Description = "Allow Microsoft Edge Update"

  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "443"
  Action = "Allow"
}

AddRule @Rule
