# Python 3.8 x64
. ..\..\AddRule.ps1

$Group = "Apps - Development - Python 3.8 x64"

$TargetPath = "$env:PROGRAMFILES\Python\*3*8\"
$TargetExecutable = "python.exe"

$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Python 3.8 x64 In"
  Description = "Allow Python 3.8 x64 inbound traffic to all ports"

  Group = $Group
  Direction = "Inbound"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Python 3.8 x64 Out"
  Description = "Allow Python 3.8 x64 outbound traffic to all ports"

  Group = $Group
  Direction = "Outbound"
  Action = "Allow"
}

AddRule @Rule
