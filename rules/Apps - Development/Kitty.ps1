# Kitty
. ..\..\AddRule.ps1

$Group = "Apps - Development - Kitty"

$Rule = @{
  TargetExecutable = "D:\software\kitty\kitty_portable.exe"

  DisplayName = "Kitty"
  Description = "Allow Kitty traffic to all ports"

  Group = $Group
  Direction = "Outbound"
  Action = "Allow"
}

AddRule @Rule
