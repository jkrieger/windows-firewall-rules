# Altium Designer
. ..\..\AddRule.ps1

$Group = "Apps - Development - Altium Designer"

$TargetPath = "$env:programfiles\Altium\"
$TargetExecutable = "X2.exe"

Write-Host $TargetPath

$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Altium Designer (allow LocalSubnet)"
  Description = "Allow local network Altium Designer traffic"

  Group = $Group
  Direction = "Outbound"
  RemoteAddress = "LocalSubnet"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Altium Designer (allow Celestial Altium Library)"
  Description = "Allow Altium Designer traffic to Celestial Altium Library servers"

  Group = $Group
  Direction = "Outbound"
  Protocol = "TCP"
  RemoteAddress = "51.68.218.24", "51.89.172.95"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Altium Designer (allow Ciiva.com)"
  Description = "Allow Altium Designer traffic to Ciiva.com"

  Group = $Group
  Direction = "Outbound"
  Protocol = "TCP"
  RemoteAddress = "51.75.242.21", "54.38.250.110", "94.23.166.79"
  Action = "Allow"
}

AddRule @Rule
