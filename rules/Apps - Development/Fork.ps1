# Fork
. ..\..\AddRule.ps1

$Group = "Apps - Development - Fork"

$Rule = @{
  TargetPath = "C:\Users\$env:UserName\AppData\Local\Fork\app-*"
  TargetExecutable = "Fork.exe"

  DisplayName = "Fork"
  Description = "Allow Fork traffic to HTTP(s) ports"

  Group = $Group
  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "80", "443"
  Action = "Allow"
}

AddRule @Rule

$Rule = @{
  TargetPath = "C:\Users\$env:UserName\AppData\Local\Fork\gitinstance\"
  TargetExecutable = "git-remote-https.exe"

  DisplayName = "Git (Fork instance)"
  Description = "Allow Git traffic to all ports"

  Group = $Group
  Direction = "Outbound"
  Protocol = "Any"
  Action = "Allow"
}

AddRule @Rule
