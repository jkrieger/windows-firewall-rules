# Git
. ..\..\AddRule.ps1

$Group = "Apps - Development - Git"


$Rule = @{
  TargetPath = "$env:programfiles\Git"
  TargetExecutable = "git-remote-http.exe"

  DisplayName = "Git"
  Description = "Allow Git traffic to All ports"

  Group = $Group
  Direction = "Outbound"
  Protocol = "TCP"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  TargetPath = "$env:programfiles\Git"
  TargetExecutable = "git-credential-manager-core.exe"

  DisplayName = "Git Credential Manager"
  Description = "Allow Git Credential Manager traffic to all ports"

  Group = $Group
  Direction = "Outbound"
  Protocol = "TCP"
  Action = "Allow"
}

AddRule @Rule
