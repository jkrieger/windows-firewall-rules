# Paint.net
. ..\..\AddRule.ps1

$Group = "Apps - Multimedia - Paint.net"

$Rule = @{
  TargetPath = "$env:programfiles\Paint.net\"
  TargetExecutable = "paintdotnet.exe"

  DisplayName = "Paint.net Update"
  Description = "Allow Paint.net updater traffic to HTTPS port"

  Group = $Group
  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "443"
  Action = "Allow"
}

AddRule @Rule
