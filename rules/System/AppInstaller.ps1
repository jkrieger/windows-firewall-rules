# AppInstaller
. ..\..\AddRule.ps1

$Rule = @{
  TargetPath = "C:\Program Files\WindowsApps\microsoft.desktopappinstaller_*\"
  TargetExecutable = "AppInstallerCLI.exe"

  Group = "System - Tools"
  DisplayName = "AppInstaller (winget)"
  Description = "Allow AppInstaller traffic to HTTPs port"
  Direction = "Outbound"
  Protocol = "TCP"
  Port = "443"
  Action = "Allow"
  Enabled = "True"
}

#AddRule @Rule


$Rule = @{
  TargetPath = "C:\Program Files\WindowsApps\microsoft.desktopappinstaller_*\"
  TargetExecutable = "winget.exe"

  Group = "System - Tools"
  DisplayName = "Winget"
  Description = "Allow Winget traffic to HTTPs port"
  Direction = "Outbound"
  Protocol = "TCP"
  Port = "443"
  Action = "Allow"
  Enabled = "True"
}


AddRule @Rule
