# Microsoft Account Sign-in Assistant
. ..\..\AddRule.ps1

$Rule = @{
  TargetExecutable = "C:\windows\system32\svchost.exe"
  Service = "wlidsvc"

  Group = "System - Tools"
  DisplayName = "Microsoft Account Sign-in Assistant"
  Description = "Allow Microsoft Account Sign-in Assistant traffic to HTTPS port"
  Direction = "Outbound"
  Protocol = "TCP"
  RemotePort = "443"
  Action = "Allow"
  Enabled = "True"
}

AddRule @Rule
