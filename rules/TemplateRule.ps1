# Template
. .\AddRule.ps1

$Group = "Template"

$Program = Get-ChildItem g:\ -ErrorAction SilentlyContinue -Filter "steam.exe" -Recurse | % { $_.FullName }
Write-Host "Found: $Program"

$Rule = @{
  Enabled = "True"
  DisplayName = "Template"
  Description = "Allow Template traffic to all ports"
  Group = "$Group"
  Program = "$Program"
  Direction = "Outbound"
  Protocol = "Any"
  Action = "Allow"
}

AddRule @Rule
