# Bonjour Sewrvice
. ..\..\AddRule.ps1

$Group = "Services - Networking - Bonjour"

$TargetExecutable = "${env:PROGRAMFILES(x86)}\Bonjour\mdnsresponder.exe"

$Rule = @{
  TargetExecutable = $TargetExecutable

  DisplayName = "Bonjour In"
  Description = "Allow Tor inbound traffic to all ports"

  Group = $Group
  Direction = "Inbound"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Bonjour Out"
  Description = "Allow Bonjour outbound traffic to all ports"

  Group = $Group
  Direction = "Outbound"
  Action = "Allow"
}

AddRule @Rule
