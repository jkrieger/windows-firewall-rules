# Tor
. ..\..\AddRule.ps1

$Group = "Services - Networking - Tor"

$TargetPath = "${env:PROGRAMFILES(x86)}\Tor"
$TargetExecutable = "tor.exe"


$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Tor (Out)"
  Description = "Allow Tor outbound traffic to all ports"

  Group = $Group
  Direction = "Outbound"
  Action = "Allow"
}

AddRule @Rule


$Rule = @{
  TargetPath = $TargetPath
  TargetExecutable = $TargetExecutable

  DisplayName = "Tor (In)"
  Description = "Allow Tor inbound traffic to all ports"

  Group = $Group
  Direction = "Inbound"
  Action = "Allow"
}

AddRule @Rule
